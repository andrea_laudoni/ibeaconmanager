//
//  iBeaconManager.h
//
//  Created by Andrea Laudoni on 10/09/14.
//  Copyright (c) 2014 Andrea Laudoni. All rights reserved.
//

#import <Foundation/Foundation.h>


@import CoreLocation;
@import CoreBluetooth;

@interface iBeaconManager : NSObject <CLLocationManagerDelegate>

+ (iBeaconManager *)sharedInstance;


@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) CLBeaconRegion *beaconRegion;
@property (strong, nonatomic) NSDictionary *myBeaconData;


@end
